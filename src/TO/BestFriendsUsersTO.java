/*
 * DTO para recojer la informacion de los usuarios con mas amigos
 */
package TO;

/**
 *
 * @author p-dur
 */
public class BestFriendsUsersTO {

    private String username;

    private int friends;

    public BestFriendsUsersTO() {
    }

    public BestFriendsUsersTO(String username, int friends) {
        this.username = username;
        this.friends = friends;
    }

    
    
    
    /**
     * Get the value of friends
     *
     * @return the value of friends
     */
    public int getFriends() {
        return friends;
    }

    /**
     * Set the value of friends
     *
     * @param friends new value of friends
     */
    public void setFriends(int friends) {
        this.friends = friends;
    }

    /**
     * Get the value of username
     *
     * @return the value of username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the value of username
     *
     * @param username new value of username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "{" + username + " - " + friends + '}';
    }
    
    

}
