/*
 * DTO para recojer la informacion de los Usuarios segun nivel
 */
package TO;

/**
 *
 * @author p-dur
 */
public class BestUserTO {

    private String username;
    private int level;

    public BestUserTO() {
    }

    public BestUserTO(String username, int level) {
        this.username = username;
        this.level = level;
    }
    
    

    /**
     * Get the value of level
     *
     * @return the value of level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Set the value of level
     *
     * @param level new value of level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Get the value of username
     *
     * @return the value of username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the value of username
     *
     * @param username new value of username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "{" + username + " - " + level + '}';
    }
    
    
}
