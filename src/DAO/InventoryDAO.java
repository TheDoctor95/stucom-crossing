/*
 * DAO para funciones del Inventario
 */
package DAO;

import exceptions.InventoryException;
import exceptions.UserException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Inventory;
import model.Item;
import model.User;

/**
 *
 * @author p-dur.
 */
public class InventoryDAO {

    //DAOS
    ItemDAO itemDAO;

    public InventoryDAO() {
        itemDAO = new ItemDAO();
    }

    //INSERTS
    public void insertUsersInventory(User u) throws SQLException, InventoryException {

        for (Inventory inventory : u.getInventory()) {
            insertInventory(u.getUsername(), inventory);
        }

    }

    public void insertInventory(String u, Inventory i) throws SQLException, InventoryException {

        if (existInventory(u, i)) {
            throw new InventoryException(1);
        }

        String insert = "INSERT INTO inventory VALUES(?,?,?)";
        PreparedStatement ps = ConectDAO.connection.prepareStatement(insert);

        ps.setString(1, u);
        ps.setString(2, i.getItem().getName());
        ps.setInt(3, i.getQuantity());

        ps.executeUpdate();

        ps.close();

    }

    public void buyItem(User u, Item i, int q) throws SQLException, InventoryException, UserException {
        ConectDAO.connection.setAutoCommit(false);
        try {
            if (canBuyItem(u, i, q)) {
                modificarMoneyComprar(u, i.getPrice() * q);
                if (existInventory(u.getUsername(), new Inventory(i))) {
                    addItemToInventory(u, i, q);
                } else {
                    insertInventory(u.getUsername(), new Inventory(i, q));
                }
                ConectDAO.connection.commit();
            } else {
                throw new InventoryException(3);
            }

        } catch (InventoryException ex) {
            throw new InventoryException(3);

        } finally {
            ConectDAO.connection.setAutoCommit(true);
        }

    }

    public void sellItem(User u, Item i, int q) throws SQLException, InventoryException, UserException {
        ConectDAO.connection.setAutoCommit(false);
        try {
            if (canSellItem(u, i, q)) {
                modificarMoneyVender(u, i.getSaleprive() * q);
                dellItemInventory(u, i, q);
                ConectDAO.connection.commit();
            } else {
                throw new InventoryException(2);
            }

        } catch (InventoryException ex) {
            throw new InventoryException(2);

        } finally {
            ConectDAO.connection.setAutoCommit(true);
        }

    }

    //UPDATES
    public void addItemToInventory(User u, Item i, int q) throws SQLException {
        String updateExp = "update inventory set quantity=quantity+" + q + " where user='" + u.getUsername() + "' AND item='"+i.getName()+"'";
        Statement st = ConectDAO.connection.createStatement();
        st.executeUpdate(updateExp);
        st.close();
    }

    public void dellItemInventory(User u, Item i, int q) throws SQLException {
        String updateExp = "update inventory set quantity=quantity-" + q + " where user='" + u.getUsername() + "' AND item='"+i.getName()+"'";
        Statement st = ConectDAO.connection.createStatement();
        st.executeUpdate(updateExp);
        st.close();
    }

    //DELETES
    //SELECTS
    public ArrayList<Inventory> getInventoryByUsername(String name) throws SQLException {
        ArrayList<Inventory> i = new ArrayList<Inventory>();

        String select = "SELECT * FROM Inventory WHERE user='" + name + "'";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(select);

        while (rs.next()) {
            Inventory in = new Inventory();
            in.setQuantity(rs.getInt("quantity"));
            Item item = itemDAO.getItemByeName(rs.getString("item"));
            in.setItem(item);
            i.add(in);
        }

        rs.close();
        st.close();

        return i;
    }

    public boolean canBuyItem(User u, Item i, int q) throws SQLException, UserException {
        boolean buy = false;

        String select = "SELECT stucoins FROM User WHERE username='" + u.getUsername() + "'";
        Statement st = ConectDAO.connection.createStatement();

        ResultSet rs = st.executeQuery(select);
        if (rs.next()) {
            if (rs.getDouble("stucoins") >= i.getPrice() * q) {
                buy = true;
            }
        } else {
            throw new UserException(3);
        }

        return buy;
    }

    public boolean canSellItem(User u, Item i, int q) throws SQLException, UserException {
        boolean buy = false;

        String select = "SELECT quantity FROM inventory WHERE user='" + u.getUsername() + "' AND item='" + i.getName() + "'";
        Statement st = ConectDAO.connection.createStatement();

        ResultSet rs = st.executeQuery(select);
        if (rs.next()) {
            if (rs.getDouble("quantity") >= q) {
                buy = true;
            }
        } else {
            throw new UserException(3);
        }

        return buy;
    }

    public void modificarMoneyComprar(User u, double q) throws SQLException {
        String updateExp = "update stucomcrossing.user set stucoins=stucoins-" + q + " where username='" + u.getUsername() + "'";
        Statement st = ConectDAO.connection.createStatement();
        st.executeUpdate(updateExp);
        st.close();
    }

    public void modificarMoneyVender(User u, double q) throws SQLException {
        String updateExp = "update stucomcrossing.user set stucoins=stucoins+" + q + " where username='" + u.getUsername() + "'";
        Statement st = ConectDAO.connection.createStatement();
        st.executeUpdate(updateExp);
        st.close();
    }

    /**
     * Funcion que comprueva si un usuario ja tiene un item en su inventario
     *
     * @param u Username
     * @param i Inventario
     * @return
     */
    public boolean existInventory(String u, Inventory i) throws SQLException {
        boolean exists = false;

        String select = "SELECT * FROM inventory WHERE user='" + u + "' AND item='" + i.getItem().getName() + "'";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(select);

        if (rs.next()) {
            exists = true;
        }
        rs.close();
        st.close();

        return exists;
    }
}
