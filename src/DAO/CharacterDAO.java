/*
 * DAO para funciones de Characters
 */
package DAO;

import enums.enumPlaces;
import enums.enumStudy;
import enums.enumTypeItem;
import exceptions.CharacterException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Character;
import model.User;

/**
 *
 * @author p-dur
 */
public class CharacterDAO {

    //INSERTS
    public void insertCharacter(Character c) throws SQLException, CharacterException {
        if (existCharacter(c)) {
            throw new CharacterException(1);
        }
        String query = "INSERT INTO stucomcrossing.character VALUES (?,?,?,?)";
        PreparedStatement ps = ConectDAO.connection.prepareStatement(query);

        ps.setString(1, c.getName());
        ps.setString(2, c.getStudy().study());
        ps.setString(3, c.getPlace().place());
        ps.setString(4, c.getPreference().name());

        ps.executeUpdate();
        ps.close();

    }

    //UPDATES
    public void modificarSitioCharacter(Character c, enumPlaces place) throws SQLException, CharacterException {
        Statement st = ConectDAO.connection.createStatement();
        try {
            String update = "update stucomcrossing.character set place='" + place.place() + "' where name='" + c.getName() + "'";
            st.executeUpdate(update);
        } catch (SQLException ex) {
            throw new CharacterException(2);
        } finally {
            st.close();
        }
    }

    //DELETES
    //SELECTS
    public boolean existCharacter(Character c) throws SQLException {
        boolean exist = false;

        String select = "SELECT * FROM stucomcrossing.character WHERE name='" + c.getName() + "'";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(select);

        if (rs.next()) {
            exist = true;
        }
        rs.close();
        st.close();

        return exist;
    }

    /**
     * Funcion que nos devuelve un Character dado su nombre
     *
     * @param name nombre del character
     * @return
     */
    public Character getCharacterByName(String name) throws SQLException {
        Character c = new Character();
        String Select = "SELECT * FROM stucomcrossing.character where name='" + name + "'";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(Select);

        if (rs.next()) {
            c.setName(name);

            enumStudy s = enumStudy.ASIX;
            s.setStudy(rs.getString("study"));
            c.setStudy(s);

            enumTypeItem ti = enumTypeItem.APUNTES;
            ti.setType(rs.getString("preference"));
            c.setPreference(ti);

            enumPlaces p = enumPlaces.AULA_22;
            p.setName(rs.getString("place"));
            c.setPlace(p);
        }

        rs.close();
        st.close();
        return c;
    }
    
    public ArrayList<Character> getCharacterForUserPlace(User u) throws SQLException {
        Statement st = ConectDAO.connection.createStatement();
        ArrayList<Character> CharactersSitioUsuario = new ArrayList<>();
        String select = "select * from stucomcrossing.character where place = '" + u.getPlace().place() + "';";
        ResultSet rs = st.executeQuery(select);

        while (rs.next()) {
            Character c = new Character();
            c.setName(rs.getString("name"));
            enumStudy s = enumStudy.ASIX;
            s.setStudy(rs.getString("study"));
            c.setStudy(s);

            enumTypeItem ti = enumTypeItem.APUNTES;
            ti.setType(rs.getString("preference"));
            c.setPreference(ti);

            enumPlaces p = enumPlaces.AULA_22;
            p.setName(rs.getString("place"));
            c.setPlace(p);
            
            CharactersSitioUsuario.add(c);
        }

        st.close();
        return CharactersSitioUsuario;
    }

}
