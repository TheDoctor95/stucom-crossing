/*
* DAO para funciones de los contactos
 */
package DAO;

import exceptions.ContactException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.util.ArrayList;
import model.Contact;
import model.User;
import model.Character;

/**
 *
 * @author p-dur
 */
public class ContactDAO {
    
    //DAOS 
    private CharacterDAO characterDAO;
    
    public ContactDAO(){
        characterDAO =  new CharacterDAO();
    }
    
    //INSERTS
    
    
    /**
     * Funcion que recibe un usuario i nos recorre su ArrayList de Contactos para añadirnoslos a la bbdd
     * @param u Usuario del cual ingresamos los contactos
     */
    public void insertUserContacts(User u) throws ContactException, SQLException{
        for (Contact contact : u.getContacts()) {
            insertContact(u.getUsername(), contact);
        }
    }
    /**
     * Funcion que recibe un username i un character i nos lo añade en la base de datos
     * @param u
     * @param c 
     */
    public void insertContact(String u, Contact c) throws ContactException, SQLException{
        
        if(existContact(u, c)){
            throw new ContactException(1);
        }
        
        String insert = "INSERT INTO stucomcrossing.contact VALUES(?,?,?,?,?)";
        PreparedStatement ps = ConectDAO.connection.prepareStatement(insert);
        
        ps.setString(1, u);
        ps.setString(2, c.getCharacter().getName());
        ps.setDate(3, Date.valueOf(c.getDate()));
        ps.setInt(4, c.getLvl());
        ps.setInt(5, c.getPoints());
        
        ps.executeUpdate();
        
        ps.close();
        
    }
    
    //UPDATES
    
    
    //DELETES
    
    
    //SELECTS
    
    public boolean existContact(String u, Contact c) throws SQLException{
        boolean exists = false;
        
        String  select = "SELECT * FROM contact WHERE user='" + u + "' AND stucomcrossing.contact.character='" + c.getCharacter().getName() + "'";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(select);
        
        if(rs.next()){
            exists=true;
        }
        rs.close();
        st.close();
        
        return exists;
    }
    
    /**
     * Funcion que nos devuelve la lista de Contactos de un usuario dado su nombre
     * @param name Nombre de Usuario
     * @return Listado de contactos del Usuario
     * @throws SQLException 
     */
    public ArrayList<Contact> getContactByUsername(String name) throws SQLException{
        
        ArrayList<Contact> contacts = new ArrayList<>(); 
        Statement st = ConectDAO.connection.createStatement();
        String select = "SELECT * FROM contact WHERE  user='"+name+"'";
        ResultSet rs = st.executeQuery(select);
        
        while (rs.next()){
            Contact c = new Contact();
            //c.setDate();
            c.setLvl(rs.getInt("level"));
            c.setPoints(rs.getInt("points"));
            c.setDate(rs.getDate("date").toLocalDate());
            c.setCharacter(characterDAO.getCharacterByName(rs.getString("character")));
            contacts.add(c);
        }
        
       rs.close();
       st.close();
        
        return contacts;
    }
    
    
}
