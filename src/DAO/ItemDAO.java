/*
* DAO para funciones de Items
 */
package DAO;

import enums.enumStudy;
import enums.enumTypeItem;
import exceptions.ItemException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Item;
import model.User;

/**
 *
 * @author p-dur
 */
public class ItemDAO {
    
    //INSERTS
    public void inserItem(Item i) throws SQLException, ItemException{
        if(existeItem(i)){
            throw new ItemException(1);
        }
        
        String query = "INSERT INTO item VALUES (?,?,?,?,?)";
        PreparedStatement ps = ConectDAO.connection.prepareStatement(query);

        ps.setString(1, i.getName());
        ps.setDouble(2, i.getPrice());
        ps.setDouble(3, i.getSaleprive());
        ps.setString(4, i.getType().name());
        ps.setString(5, i.getStyle().study());
        

        ps.executeUpdate();
        ps.close();
        
        
    }
    
    //UPDATES
    
     public void modificarItemPrecio(Item i, Double price) throws SQLException, ItemException {
        Statement st = ConectDAO.connection.createStatement();
        try{
        String updatePlace = "update stucomcrossing.item set price='" + price + "', saleprice='"+(price/2)+"'  where name='" + i.getName() + "'";
        st.executeUpdate(updatePlace);
        }catch (SQLException ex){
            throw new ItemException(2);
        }
        st.close();

    }

    
    //DELETES
    
    
    //SELECTS
    
    public Item getItemByeName(String name) throws SQLException{
        Item i = new Item();
        
        String select = "SELECT * FROM Item WHERE name='"+name+"'";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(select);
        
        if(rs.next()){
            i.setName(name);
            i.setPrice(rs.getDouble("price"));
            
            enumTypeItem t = enumTypeItem.APUNTES;
            t.setType(rs.getString("type"));
            i.setType(t);
            
            enumStudy s = enumStudy.ASIX;
            s.setStudy(rs.getString("style"));
            i.setStyle(s);
            
        }
        st.close();
        rs.close();
        return i;
    }
    
    public boolean existeItem(Item i) throws SQLException{
        boolean exist = false;
        
         
        String select = "SELECT * FROM Item WHERE name='"+i.getName()+"'";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(select);
        
        if(rs.next()){
            exist=true;
        }
        rs.close();
        st.close();
        return exist;
    }
    
    
    public List<Item> itemsNotUser(User u) throws SQLException{
         String select = "select * from item where item.name not in (select item from inventory where user = '" + u.getUsername() + "')";
        Statement st = ConectDAO.connection.createStatement();
        ResultSet rs = st.executeQuery(select);
        List<Item> noInventory = new ArrayList<>();
        while (rs.next()) {
            enumTypeItem ti = enumTypeItem.APUNTES;
            ti.setType(rs.getString("type"));
            
            enumStudy s = enumStudy.ASIX;
            s.setStudy(rs.getString("style"));
            
            Item i = new Item(rs.getString("name"), rs.getInt("price"), ti, s);
            noInventory.add(i);
        }
        rs.close();
        st.close();
        return noInventory;
    }
}
