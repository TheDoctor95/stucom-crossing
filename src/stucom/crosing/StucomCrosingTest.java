/*
 * Classe para et
 */
package stucom.crosing;

import DAO.CharacterDAO;
import DAO.ConectDAO;
import DAO.InventoryDAO;
import DAO.ItemDAO;
import DAO.UserDAO;
import TO.BestFriendsUsersTO;
import TO.BestUserTO;
import enums.enumPlaces;
import enums.enumStudy;
import enums.enumTypeItem;
import exceptions.CharacterException;
import exceptions.ContactException;
import exceptions.InventoryException;
import exceptions.ItemException;
import exceptions.UserException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Inventory;
import model.Item;
import model.User;
import model.Character;
import model.Contact;

/**
 *
 * @author p-dur
 */
public class StucomCrosingTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {

            //Creacion de todas las bariables DAO.
            ConectDAO cDAO = new ConectDAO();
            UserDAO uDAO = new UserDAO();
            ItemDAO itDAO = new ItemDAO();
            CharacterDAO chDAO = new CharacterDAO();
            InventoryDAO inDAO = new InventoryDAO();

            // ===> CONEXION BBDD
            System.out.println("CONECTANDO A LA BBDD...");
            cDAO.conectar();
            System.out.println(" + conexion realizada correctamente");
            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                          COMPROVACION DE INSERTS                            |");
            System.out.println("-------------------------------------------------------------------------------");

            System.out.println("INSERTANDO CHARACTERS EN LA BBDD...");

            Character c1 = new Character("Pepito", enumStudy.SIMIX, enumPlaces.AULA_22, enumTypeItem.MUEBLES);
            Character c2 = new Character("Juenete", enumStudy.SIMIX, enumPlaces.CAFETERIA, enumTypeItem.MUEBLES);
            Character c3 = new Character("Gonzalete", enumStudy.SIMIX, enumPlaces.RECEPCION, enumTypeItem.MUEBLES);
            Character c4 = new Character("Fury", enumStudy.SIMIX, enumPlaces.SALA_ESTUDIO, enumTypeItem.MUEBLES);
            Character c5 = new Character("Albertet", enumStudy.SIMIX, enumPlaces.SALA_PROFESORES, enumTypeItem.MUEBLES);
            Character c6 = new Character("IronFace", enumStudy.SIMIX, enumPlaces.SALA_ESTUDIO, enumTypeItem.MUEBLES);

            try {
                System.out.println(" > Insertando character: " + c1.getName());
                chDAO.insertCharacter(c1);
                System.out.println(" + Character insertado correctamente");
            } catch (CharacterException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println(" > Insertando character: " + c1.getName());
                chDAO.insertCharacter(c1);
                System.out.println(" + Character insertado correctamente");
            } catch (CharacterException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println(" > Insertando characters extras necesarios para la practica...");
                chDAO.insertCharacter(c2);
                chDAO.insertCharacter(c3);
                chDAO.insertCharacter(c4);
                chDAO.insertCharacter(c5);
                chDAO.insertCharacter(c6);
                System.out.println(" + Characters extras insertados correctamente");
            } catch (CharacterException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("INSERTANDO ITEMS EN LA BBDD...");

            Item i1 = new Item("Apuntes BBDD", 50, enumTypeItem.APUNTES, enumStudy.ASIX);
            Item i2 = new Item("Bocadillo de Chorizo", 15, enumTypeItem.COMIDA, enumStudy.BATCHILLERATO);
            Item i3 = new Item("Silla", 100, enumTypeItem.MUEBLES, enumStudy.DAW);
            Item i4 = new Item("HOW TO GE RICH?", 100, enumTypeItem.LIBROS, enumStudy.FINANZAS);
            Item i5 = new Item("Percheros", 50, enumTypeItem.MUEBLES, enumStudy.DAW);
            Item i6 = new Item("CAFE", 100, enumTypeItem.BEBIDA, enumStudy.DAM);
            Item i7 = new Item("MACs", 100, enumTypeItem.MUEBLES, enumStudy.SIMIX);
            Item i8 = new Item("TUTORIAL JHIPSER", 400, enumTypeItem.VIDEOTUTORIALES, enumStudy.DAW);

            //Insertando un usuario nuevo
            try {
                System.out.println(" > Insertando item: " + i1.getName());
                itDAO.inserItem(i1);
                System.out.println(" + Item insertado correctamente");
            } catch (ItemException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                System.out.println(" > Insertando item: " + i1.getName());
                itDAO.inserItem(i1);
                System.out.println(" + Item insertado correctamente");
            } catch (ItemException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println(" > Insertando items necesarios para la practica...");
                itDAO.inserItem(i2);
                itDAO.inserItem(i3);
                itDAO.inserItem(i4);
                itDAO.inserItem(i5);
                itDAO.inserItem(i6);
                itDAO.inserItem(i7);
                itDAO.inserItem(i8);
                System.out.println(" + Items necesarios insertados correctamente");
            } catch (ItemException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");

            System.out.println("");
            // ===> REGISTRAR USUARIO

            System.out.println("INSERTANDO USUARIOS EN LA BBDD...");

            User u1 = new User("TheDoctor", "P@ssw0rd", enumPlaces.AULA_22);
            //Al usuario 1 le damos mas dinero para poder hacer la comprovacion de las compras posteriores
            u1.setStucoins(2000);
            u1.addInventory(new Inventory(i8, 10));
            u1.addInventory(new Inventory(i2, 1));
            u1.addInventory(new Inventory(i4, 4));
            u1.addInventory(new Inventory(i6, 6));
            u1.addContacts(new Contact(c6, LocalDate.now(), 0, 0));
            u1.addContacts(new Contact(c2, LocalDate.now(), 0, 0));
            u1.addContacts(new Contact(c5, LocalDate.now(), 0, 0));
            u1.addContacts(new Contact(c3, LocalDate.now(), 0, 0));
            u1.addContacts(new Contact(c1, LocalDate.now(), 0, 0));

            User u2 = new User("Whoger", "P@ssw0rd2", enumPlaces.SALA_ESTUDIO);
            u2.addInventory(new Inventory(i1, 10));
            u2.addInventory(new Inventory(i7, 1));
            u2.addInventory(new Inventory(i3, 4));
            u2.addInventory(new Inventory(i5, 6));
            u2.addContacts(new Contact(c5, LocalDate.now(), 0, 0));
            u2.addContacts(new Contact(c3, LocalDate.now(), 0, 0));
            u2.addContacts(new Contact(c1, LocalDate.now(), 0, 0));

            User u3 = new User("MeChorea", "P@ssw0rd3", enumPlaces.SALA_ESTUDIO);
            u3.addInventory(new Inventory(i1, 10));
            u3.addInventory(new Inventory(i7, 1));
            u3.addInventory(new Inventory(i3, 4));
            u3.addInventory(new Inventory(i5, 6));
            u3.addContacts(new Contact(c5, LocalDate.now(), 0, 0));
            u3.addContacts(new Contact(c1, LocalDate.now(), 0, 0));

            //Insertando un usuario nuevo
            try {
                System.out.println(" > Insertando usuario: " + u1.getUsername());
                uDAO.insertUser(u1);
                System.out.println(" + Usuario insertado correctamente");
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (ContactException ex) {
                System.out.println(ex.getMessage());
            }

            //Insertando un usuario repetido
            try {
                System.out.println(" > Insertando usuario: " + u1.getUsername());
                uDAO.insertUser(u1);
                System.out.println(" + Usuario insertado correctamente");
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (ContactException ex) {
                System.out.println(ex.getMessage());
            }

            //Insertando un usuarios extras
            try {
                System.out.println(" > Insertando extras necesarios para la practica... ");
                uDAO.insertUser(u2);
                uDAO.insertUser(u3);

                System.out.println(" + Usuarios Extras añadidos correctamente");
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (ContactException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");
            System.out.println("");
            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                              MOSTRANDO USERS                                |");
            System.out.println("-------------------------------------------------------------------------------");
            //SELECT USER
            // El select del user tiene el select del de sus Contactos i su Inventario
            try {
                System.out.println("> Buscando usuario: " + u1.getUsername());
                User u1Find = uDAO.findUserByName(u1.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u1Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                System.out.println("> Buscando usuario: " + u2.getUsername());
                User u2Find = uDAO.findUserByName(u2.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u2Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                System.out.println("> Buscando usuario: " + u3.getUsername());
                User u3Find = uDAO.findUserByName(u3.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u3Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");
            System.out.println("");
            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                              VALIDAR USUARIO                                |");
            System.out.println("-------------------------------------------------------------------------------");

            String username1 = u1.getUsername();

            String username3 = "Pablo";
            String password1 = u1.getPassword();

            //Validacion correcta
            System.out.println(" > Validando usuario usuario: username = " + username1 + " password = " + password1);
            System.out.println(" -> " + uDAO.validateUser(username1, password1));
            //Validacion incorrecta 
            System.out.println(" > Validando usuario usuario: username = " + username3 + " password = " + password1);
            System.out.println(" -> " + uDAO.validateUser(username3, password1));
            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                             MODIFICAR USUARIO                               |");
            System.out.println("-------------------------------------------------------------------------------");

            try {
                u1.setUsername("TheDoctor95");
                u1.setPassword("contrasenya");
                System.out.println(" > Modificando user: " + username1 + "=>" + u1.getUsername() + " " + password1 + "=>" + u1.getPassword());
                uDAO.modificarUsuario(username1, u1);
                System.out.println(" + Usuario modificado correctamente");
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");

            System.out.println("");

            try {

                System.out.println(" > Moviendo  user: " + u3.getUsername() + " " + u3.getPlace().place() + "=>" + enumPlaces.SALA_ESTUDIO.place());
                uDAO.modificarSitioUsu(u3, enumPlaces.SALA_ESTUDIO);
                System.out.println(" + Usuario movido correctamente");
                u3 = uDAO.findUserByName(u3.getUsername());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                          MOVIENDO CHRACTER                               |");
            System.out.println("-------------------------------------------------------------------------------");
            try {
                System.out.println(" > Moviendo character:" + " " + c4.getName() + " de " + c4.getPlace() + " a " + enumPlaces.SALA_PROFESORES);
                chDAO.modificarSitioCharacter(c4, enumPlaces.SALA_PROFESORES);
            } catch (CharacterException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                          MODIFICAR PRECIO                                   |");
            System.out.println("-------------------------------------------------------------------------------");

            try {
                double price = 25.60;
                System.out.println(" > Modificando precio de " + i4.getName() + " de " + i4.getPrice() + " a " + price);
                itDAO.modificarItemPrecio(i4, price);
            } catch (ItemException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                               SELECTS USERS                                 |");
            System.out.println("-------------------------------------------------------------------------------");

            System.out.println(" > Seleccionando Characters del mismo sitio que " + u1.getUsername());
            List<Character> chs = chDAO.getCharacterForUserPlace(u1);
            for (Character ch : chs) {
                System.out.println(ch);
            }

            System.out.println(" > Seleccionando characters que no conoce " + u2.getUsername());
            List<Character> chs2 = uDAO.noContactos(u2);
            for (Character ch : chs2) {
                System.out.println(ch);
            }

            System.out.println(" > Seleccionando objetos que no tiene " + u3.getUsername());
            List<Item> its = itDAO.itemsNotUser(u3);
            for (Item it : its) {
                System.out.println(it);
            }
            //List<Character> chs3 = 

            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                                    TIENDA                                   |");
            System.out.println("-------------------------------------------------------------------------------");

            try {
                int q = 1;
                System.out.println(" > Comprando new    item: " + u1.getUsername() + " buys " + q + " " + i3.getName());
                inDAO.buyItem(u1, i3, q);
                System.out.println(" + Item comprado correctamente");
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println("> Buscando usuario: " + u1.getUsername());
                User u1Find = uDAO.findUserByName(u1.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u1Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                int q = 1;
                System.out.println(" > Comprando new    item: " + u1.getUsername() + " buys " + q + " " + i3.getName());
                inDAO.buyItem(u1, i3, q);
                System.out.println(" + Item comprado correctamente");
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println("> Buscando usuario: " + u1.getUsername());
                User u1Find = uDAO.findUserByName(u1.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u1Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                int q = 9;
                System.out.println(" > Comprando new    item: " + u1.getUsername() + " buys " + q + " " + i3.getName());
                inDAO.buyItem(u1, i3, q);
                System.out.println(" + Item comprado correctamente");
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println("> Buscando usuario: " + u1.getUsername());
                User u1Find = uDAO.findUserByName(u1.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u1Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                int q = 2;
                System.out.println(" > Comprando new    item: " + u1.getUsername() + " sells " + q + " " + i3.getName());
                inDAO.sellItem(u1, i3, q);
                System.out.println(" + Item vendido correctamente");
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println("> Buscando usuario: " + u1.getUsername());
                User u1Find = uDAO.findUserByName(u1.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u1Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                int q = 10;
                System.out.println(" > Comprando new    item: " + u1.getUsername() + " sells " + q + " " + i3.getName());
                inDAO.sellItem(u1, i3, q);
                System.out.println(" + Item vndido correctamente");
            } catch (InventoryException ex) {
                System.out.println(ex.getMessage());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                System.out.println("> Buscando usuario: " + u1.getUsername());
                User u1Find = uDAO.findUserByName(u1.getUsername());
                System.out.println(" + Usuario encontrado:");
                System.out.println(u1Find.toString());
            } catch (UserException ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                                INTERCANVIOS                                 |");
            System.out.println("-------------------------------------------------------------------------------");

            System.out.println("===============================================================================");

            System.out.println("");

            System.out.println("-------------------------------------------------------------------------------");
            System.out.println("|                                   RANKINGS                                  |");
            System.out.println("-------------------------------------------------------------------------------");

            System.out.println(" > RANKING BEST USERS");
            List<BestUserTO> bu = uDAO.ranking();
            for (BestUserTO bestUserTO : bu) {
                System.out.println(bestUserTO);
            }

            System.out.println(" > RANKING BEST FRIENDS");
            List<BestFriendsUsersTO> bf = uDAO.rankingAmigos();
            for (BestFriendsUsersTO bestFriendsUsersTO : bf) {
                System.out.println(bestFriendsUsersTO);
            }

            System.out.println("===============================================================================");

            System.out.println("");

            // ===> DESCONEXION BBDD
            System.out.println("DESCONECTANDO DE LA BBDD...");
            cDAO.descoectar();
            System.out.println(" + desconexion realizada correctamente");
            System.out.println("===============================================================================");

        } catch (SQLException ex) {
            System.out.println(" - Error de conexion/desconexion: " + ex.getMessage());
        }

    }

}
