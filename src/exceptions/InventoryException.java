/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author p-dur
 */
public class InventoryException extends Exception{
    private int code;

    public InventoryException(int code) {
        this.code = code;
    }
    
    @Override
    public String getMessage() {
        switch(this.code){
            case 1:
                return " - El usuario ja tiene ese item en su inventario";
            case 2:
                return " - El usuario no tiene suficientes items de este objeto para vender";
            
            case 3:
                return " - El usuario no tiene suficiente dinero para comprar este  item ";
                
            default:
                return "CODIGO DE ERROR INCORRECTO";
                
        }
    }
}
