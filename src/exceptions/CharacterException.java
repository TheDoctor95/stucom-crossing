/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author p-dur
 */
public class CharacterException extends Exception{
    
     private int code;

    public CharacterException(int code) {
        this.code = code;
    }
    
    @Override
    public String getMessage() {
        switch(this.code){
            case 1:
                return " - El character ja existe";
            
                
            default:
                return "CODIGO DE ERROR INCORRECTO";
                
        }
    }
    
}
