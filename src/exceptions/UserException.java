/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author p-dur
 */
public class UserException extends Exception{
    private int code;

    public UserException(int code) {
        this.code = code;
    }

    
    
    @Override
    public String getMessage() {
        switch(this.code){
            case 1:
                return " - El usuario ja existe";
            case 2:
                return " - No se ha podido actualizar el usuario";
            case 3:
                return " - No existe el usuario";
                
            default:
                return "CODIGO DE ERROR INCORRECTO";
                
        }
    }
    
    
    
}
