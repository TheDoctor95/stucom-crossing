/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;

/**
 *
 * @author p-dur
 */
public class Contact {
    
    private Character character;
    private LocalDate date;
    private int lvl;
    private int points;

    public Contact() {
    }

    public Contact(Character character) {
        this.character = character;
        this.lvl = 1;
        this.points=0;
        this.date = LocalDate.now();
    }

    
    public Contact(Character character, LocalDate date, int lvl, int points) {
        this.character = character;
        this.date = date;
        this.lvl = lvl;
        this.points = points;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "\n {" + character + "} date: " + date + ", lvl: " + lvl + ", points: " + points + '}';
    }
    
    
    
    
    
}
