/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import enums.enumPlaces;
import java.util.ArrayList;

/**
 *
 * @author p-dur
 */
public class User {

    private String username;
    private String password;
    private int stucoins;
    private int lvl;
    private enumPlaces place;
    private int points;
    // 
    private ArrayList<Inventory> inventory;
    private ArrayList<Contact> contacts;

    public User() {
        this("", "", 0, 0, enumPlaces.RECEPCION, 0);
    }

    public User(String username, String password, enumPlaces place) {
        this(username, password, 100, 0, place, 0);
    }

    public User(String username, String password, int stucoins, int lvl, enumPlaces place, int points) {
        this.username = username;
        this.password = password;
        this.stucoins = stucoins;
        this.lvl = lvl;
        this.place = place;
        this.points = points;
        inventory = new ArrayList<>();
        contacts = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStucoins() {
        return stucoins;
    }

    public void setStucoins(int stucoins) {
        this.stucoins = stucoins;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public enumPlaces getPlace() {
        return place;
    }

    public void setPlace(enumPlaces place) {
        this.place = place;
    }

    public ArrayList<Inventory> getInventory() {
        return inventory;
    }

    public void setInventory(ArrayList<Inventory> inventory) {
        this.inventory = inventory;
    }

    /**
     * Funcion que recibe un Inventory para añadirlo al Inventario del Usuario
     *
     * @param inventory
     */
    public void addInventory(Inventory inventory) {
        this.inventory.add(inventory);
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    /**
     * Funcion que recibe un contacto para añadirlo a la lista de contactos del
     * Usuario
     *
     * @param contact Contacto a añadir a la lista
     */
    public void addContacts(Contact contact) {
        this.contacts.add(contact);
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return    "===========USER==========="
                + "\n |=> Username " + username
                + "\n |=> Password " + password
                + "\n |=> Stucoins " + stucoins
                + "\n |=> lvl " + lvl
                + "\n |=> Place " + place.place()
                + "\n |=> Points " + points
                + "\n |=>INVENTORY: \n"
                + inventory.toString()
                + "\n |=>CONTACTS: " + contacts.toString();
    }

}
