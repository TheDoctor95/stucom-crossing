/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author p-dur
 */
public class Inventory {
    
    
    private Item item;
    private int quantity;

    public Inventory() {
    }

    public Inventory(Item item) {
        this.item = item;
    }
    

    public Inventory(Item item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "   " +quantity + " " + item.toString() + "\n";
    }
    
    
}
