/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import enums.enumStudy;
import enums.enumTypeItem;

/**
 *
 * @author p-dur
 */
public class Item {

    private String name;
    private double price;
    private double saleprive;
    private enumTypeItem type;
    private enumStudy style;

    public Item() {
    }

    public Item(String name, double price, enumTypeItem type, enumStudy style) {
        this.name = name;
        this.price = price;
        //determinamos el saleprice como la mitad del saleprice
        this.saleprive = price/2;
        this.type = type;
        this.style = style;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
        // Al modificar el price recalculamos el saleprice como la mitad del price
        this.saleprive = price/2;
    }

    public double getSaleprive() {
        return saleprive;
    }

   

    public enumTypeItem getType() {
        return type;
    }

    public void setType(enumTypeItem type) {
        this.type = type;
    }

    public enumStudy getStyle() {
        return style;
    }

    public void setStyle(enumStudy style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return name + "{price=" + price + ", saleprive=" + saleprive + ", type=" + type + ", style=" + style + '}';
    }
    
    
    
}
