/*
 *
 */
package model;

import enums.enumPlaces;
import enums.enumStudy;
import enums.enumTypeItem;

/**
 *
 * @author p-dur
 */
public class Character {
    
    private String name;
    private enumStudy study;
    private enumPlaces place;
    private enumTypeItem preference;

    public Character() {
    }

    public Character(String name, enumStudy study, enumPlaces place, enumTypeItem preference) {
        this.name = name;
        this.study = study;
        this.place = place;
        this.preference = preference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public enumStudy getStudy() {
        return study;
    }

    public void setStudy(enumStudy study) {
        this.study = study;
    }

    public enumPlaces getPlace() {
        return place;
    }

    public void setPlace(enumPlaces place) {
        this.place = place;
    }

   
    public enumTypeItem getPreference() {
        return preference;
    }

    public void setPreference(enumTypeItem preference) {
        this.preference = preference;
    }

    @Override
    public String toString() {
        return name.toUpperCase() + " => study=" + study + ", place=" + place + ", preference=" + preference;
    }
    
    
    
}
