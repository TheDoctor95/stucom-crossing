/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enums;

/**
 *
 * @author p-dur
 */
public enum enumTypeItem {
    COMIDA("COMIDA"),
    BEBIDA("BEBIDA"),
    APUNTES("APUNTES"),
    LIBROS("LIBROS"),
    VIDEOTUTORIALES("VIDEOTUTORIALES"),
    MUEBLES("MUEBLES");
    
    private String type;

    enumTypeItem(String type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String type(){
        return this.type;
    }
    
}
