/*
 * He creado una enumeracion para poner los posibles places del 
 */
package enums;

/**
 *
 * @author p-dur
 */
public enum enumPlaces {
    AULA_22("aula 22"),
    SALA_PROFESORES("Sala profesores"),
    CAFETERIA("Cafeteria"),
    SALA_ESTUDIO("Sala estudio"),
    RECEPCION("Recepcion")
    ;
    
    private String name;

    enumPlaces(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
    public String place(){
        return name;
    }
    
}