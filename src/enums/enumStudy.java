/*
 * ENUMERACION DE TIPOS DE ESTUDIO POSIBLES
 */
package enums;

/**
 *
 * @author p-dur
 */
public enum enumStudy {
    BATCHILLERATO("BATCHILLERATO"), 
    MARQUETIING("MARQUETING"),
    DAM("DAM"),
    DAW("DAW"),
    ASIX("ASIX"),
    SIMIX("SIMIX"),
    COMERCIO("COMERCIO"),
    FINANZAS("FINANZAS"),
    PFI("PFI");
    
    
    private String study;

    enumStudy(String study) {
        this.study = study;
    }

    public void setStudy(String study) {
        this.study = study;
    }
    
    
    
    public String study(){
        return this.study;
    }
    
    
}
